package com.halas;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.concurrent.TimeUnit;

class TwitchBot {
    private static final Logger LOG = LogManager.getLogger(TwitchBot.class);
    private static final String HOME_PAGE = "https://www.twitch.tv/tomatosmuchachos";
    private static final String PATH_TO_DRIVER = "src/main/resources/chromedriver.exe";
    private static final String WEB_DRIVER_NAME = "webdriver.chrome.driver";
    private static final String LOGIN = "furrLion";
    private static final String PASSWORD = "423489123789";
    private static final int COUNT_MESSAGE = 100;
    private static WebDriver driver;
    private static WebElement element;

    @BeforeAll
    static void initObjects() {
        System.setProperty(WEB_DRIVER_NAME, PATH_TO_DRIVER);
        driver = new ChromeDriver();
        driver.manage().timeouts()
                .implicitlyWait(10, TimeUnit.SECONDS);
        driver.get(HOME_PAGE);
    }

    @Test
    void test() throws InterruptedException {
        LOG.info("Title: " + driver.getTitle());
        element = driver.findElement(By.cssSelector("button[data-a-target=\"login-button\"]"));
        element.click();
        element = driver.findElement(By.cssSelector("input[autocomplete=\"username\"]"));
        element.sendKeys(LOGIN);
        element = driver.findElement(By.cssSelector("input[autocomplete=\"current-password\"]"));
        element.sendKeys(PASSWORD);
        element = driver.findElement(By.cssSelector("[data-a-target=\"passport-login-button\"]"));
        element.click();
        TimeUnit.SECONDS.sleep(3);
        element = driver.findElement(By.cssSelector("button#mature-link"));
        element.click();

        for(int i = 0; i < COUNT_MESSAGE; i++) {
            element = driver.findElement(By.cssSelector("[data-test-selector=\"chat-input\"]"));
            element.sendKeys("@TomatosMuchachos  hello bro");
            element = driver.findElement(By.cssSelector("[data-test-selector=\"chat-send-button\"]"));
            element.click();
            TimeUnit.SECONDS.sleep(30);
        }
    }

    @AfterAll
    static void closeObjects() {
        driver.quit();
    }
}
