package com.halas;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.concurrent.TimeUnit;

import static org.junit.jupiter.api.Assertions.*;

class YahooTest {
    private static final Logger LOG = LogManager.getLogger(YahooTest.class);
    private static final String HOME_PAGE = "https://www.yahoo.com/";
    private static final String PATH_TO_DRIVER = "src/main/resources/chromedriver.exe";
    private static final String WEB_DRIVER_NAME = "webdriver.chrome.driver";
    private static WebDriver driver;
    private static WebElement element;

    @BeforeAll
    static void initializeObjects() {
        System.setProperty(WEB_DRIVER_NAME, PATH_TO_DRIVER);
        driver = new ChromeDriver();
        driver.manage().timeouts()
                .implicitlyWait(10, TimeUnit.SECONDS);
        driver.get(HOME_PAGE);
    }

    @Test
    void testPageTitle() {
        LOG.info(driver.getTitle());
        assertNotNull(driver.getTitle());
    }

    @Test
    void testGoToFoundElementByName() throws InterruptedException {
        element = driver.findElement(By.name("p"));
        element.sendKeys("Apple");
        element.submit();
        element = driver.findElement(By.cssSelector(".compList ul li:nth-child(2) a"));
        element.click();

        assertEquals("Images", driver.findElement(By.className("active")).getText());
    }

    @AfterAll
    static void closeResources() {
        driver.quit();
    }
}